import os

#os.system('apt list > packages.txt')

packages = []
depends_on = {}

with open('packages.txt', 'rb') as f:
  for line in f:
    line = line.decode('ascii')
    if (line == 'Listing...'):
      continue
    line = line.split('/')[0]
    os.system(f'apt-rdepends {line} > dependency-tree.txt')
    last_package = ''
    with open('dependency-tree.txt', 'rb') as dt:
      for l in dt:
        l = l.decode('ascii')
        if(l[0] != ' '):
          last_package = l.replace('\n', '').replace(' ', '')
          depends_on[last_package] = []
        else:
          dependency = l.split('Depends: ')[1].split('(')[0].replace('\n', '').replace(' ', '')
          if not dependency in depends_on[last_package]:
            depends_on[last_package].append(dependency)

print('(define (problem install-debian-packages)')
print('(:domain dependencies-manager)')
print('(:objects')
for k in depends_on:
  print(k)
print(')')
print('(:init')
for k in depends_on:
  print(f'(package {k})')
for k in depends_on:
  for j in depends_on[k]:
    print(f'(depends-on {k} {j})')
print('(= (total-cost) 0))')
print('(:goal (and (installed 0ad-data-common)))')
print('(:metric minimize (total-cost)))')
